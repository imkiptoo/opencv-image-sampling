import cv2
import sys

cascPath = sys.argv[1]
faceCascade = cv2.CascadeClassifier(cascPath)
video_capture = cv2.VideoCapture(0)

# Parameter park 1
x1 = 150
x2 = 220
y1 = 160
y2 = 230

# Parameter park 2
x11 = 340
x12 = 220
y11 = 350
y12 = 230

# Parameter park 3
x13 = 520
x23 = 220
y13 = 530
y23 = 230

while True:
    # Capture frame-by-frame
    ret, frame = video_capture.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    rect1 = cv2.rectangle(frame, (x1, x2), (y1, y2), (255, 0, 0), 2)
    cv2.putText(frame,"PARK 1", (y1, y2), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
    rect1 = cv2.rectangle(frame, (x11, x12), (y11, y12), (255, 0, 0), 2)
    cv2.putText(frame,"PARK 2", (y11, y12), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
    rect1 = cv2.rectangle(frame, (x13, x23), (y13, y23), (255, 0, 0), 2)
    cv2.putText(frame,"PARK 3", (y13, y23), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
    vehicle = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

# Vehicle detection
for (x, y, w, h) in vehicle:
    cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
    if cv2.KeyPoint((150.1,160.1),(x,y)):
        cv2.putText(frame,"PARK OCCUPIED", (x,y), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
        # When both region of interest overlap, display PARK OCCUPIED
        if x < x1:
            if y < y1:
                cv2.putText(frame, "PARK 1 OCCUPIED", (x1, y1), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
            if x < x11:
                if y < y11:
                    cv2.putText(frame, "PARK 2 OCCUPIED", (x11, y11), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
                if x < x13:
                    if y < y13:
                        cv2.putText(frame, "PARK 3 OCCUPIED", (x13, y13), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 1)
                    # Display the resulting frame     cv2.imshow('Video', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
